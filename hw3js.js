// Теоритичне завдання
// Це зручний спосіб отримання окремих значень зі складних даних, таких як масиви або об'єкти, і присвоєння їх змінним за одну операцію.


// Завдання 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const mergedClients = [...new Set([...clients1, ...clients2])];

console.log(mergedClients);

// Завдання 2
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    // інші об'єкти
];

const charactersShortInfo = characters.map(({ name, lastName, age }) => ({ name, lastName, age }));

console.log(charactersShortInfo);

// Завдання 3
const user1 = {
    name: "John",
    years: 30
};

let { name: userName, years: userAge, isAdmin = false } = user1;

console.log(userName, userAge, isAdmin);

// Завдання 4
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
};
const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
};
const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
};

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(fullProfile);

// Завдання 5
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
};

const updatedBooks = [...books, bookToAdd];

console.log(updatedBooks);

// Завдання 6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

const updatedEmployee = { ...employee, age: 30, salary: 5000 };

console.log(updatedEmployee);

// Завдання 7
const array = ['value', () => 'showValue'];
const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'